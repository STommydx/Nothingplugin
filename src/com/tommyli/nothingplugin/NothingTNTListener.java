package com.tommyli.nothingplugin;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Egg;
import org.bukkit.entity.FallingBlock;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.entity.Snowball;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

public class NothingTNTListener implements Listener{
	public static Nothingplugin plugin;
	
	public NothingTNTListener(Nothingplugin instance){
		plugin = instance;
	}
	
	@EventHandler(priority = EventPriority.HIGH)
	public void onBlockPlace(BlockPlaceEvent event){
		if (event.isCancelled()) return;
		Material block = event.getBlock().getType();
		Player player = event.getPlayer();
		if (block == Material.TNT){
			if (!player.isOp()) {
				player.sendMessage(ChatColor.YELLOW + "[Warning] You have no permission to place TNT on this server.");
				if (plugin.getConfig().getBoolean("broadcast"))
					Bukkit.getServer().broadcastMessage(ChatColor.GOLD + player.getDisplayName() + " who is gay tried to place a TNT block.");
				else if (plugin.getConfig().getBoolean("msgtoop"))
					for (Player onlinePlayer : Bukkit.getServer().getOnlinePlayers())
						if (onlinePlayer.isOp())
							onlinePlayer.sendMessage(ChatColor.GOLD + player.getDisplayName() + " who is gay tried to place a TNT block.");
				event.setCancelled(true);
			}
		}else if (block == Material.CAKE_BLOCK) event.setCancelled(true);

	}
	
	@EventHandler
	public void onProjectileHit(ProjectileHitEvent event){
		Projectile pro = event.getEntity();
		if(pro.getShooter() instanceof Player){
			Player shooter = (Player) pro.getShooter();
			if (shooter.isOp()){
				if (pro instanceof Snowball)
					if (plugin.getConfig().getBoolean("expsnowball"))
					pro.getWorld().createExplosion(pro.getLocation(), (float) plugin.getConfig().getDouble("snowpower"));
				if (pro instanceof Egg)
					pro.getLocation().getBlock().setType(Material.LAVA);
			}
		}
	}

	
	@EventHandler(priority = EventPriority.HIGH)
	public void onEntityExplode(EntityExplodeEvent event){
		if (!plugin.getConfig().getBoolean("expblock"))
			event.blockList().clear();
	}
	
	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent event){
		Player shooter = event.getPlayer();
		if (event.getItem() != null)
			if (event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK){
				if (event.getItem().getType() == Material.CLAY_BRICK){
					reduceAmount (shooter);
					final Item titem = shooter.getWorld().dropItem(shooter.getEyeLocation(), new ItemStack(Material.BRICK, 1));
					titem.setVelocity(shooter.getLocation().getDirection().multiply(1.5));
					titem.getWorld().playSound(titem.getLocation(), Sound.ANVIL_USE, 4F, 2F);
					titem.setPickupDelay(100000);
					Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable(){

						@Override
						public void run() {
							titem.getWorld().createExplosion(titem.getLocation(), (float) plugin.getConfig().getDouble("brickpower"));
							titem.remove();
						}
					
					}, 30L);
				}else if (event.getItem().getType() == Material.CAKE){
					reduceAmount (shooter);
					FallingBlock fb = shooter.getWorld().spawnFallingBlock(shooter.getEyeLocation(), Material.CAKE_BLOCK, (byte) 0);
					fb.setVelocity(shooter.getLocation().getDirection().multiply(1.5));
					fb.setDropItem(false);
				}
			}
	}
	
	public void reduceAmount (Player player){
		ItemStack is = player.getItemInHand();
		if (player.getGameMode() != GameMode.CREATIVE) is.setAmount(is.getAmount() - 1);
		player.setItemInHand(is);
	}

}
