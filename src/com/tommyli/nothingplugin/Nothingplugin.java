package com.tommyli.nothingplugin;

import java.util.Random;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class Nothingplugin extends JavaPlugin{
	public final Logger logger = Logger.getLogger("Minecraft");
	public static Nothingplugin plugin;
	
	@Override
	public void onDisable(){
		PluginDescriptionFile pdfFile = this.getDescription();
		this.logger.info(pdfFile.getName() + " has been disabled.");
	}
	
	@Override
	public void onEnable(){
		PluginDescriptionFile pdfFile = this.getDescription();
		this.logger.info(pdfFile.getName() + " v." + pdfFile.getVersion() + " has been enabled.");
		getConfig().options().copyDefaults(true);
		saveConfig();
		PluginManager manager = this.getServer().getPluginManager();
		manager.registerEvents(new NothingTNTListener(this), this);
	}
	
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args){
		Player player = (Player) sender;
		if (commandLabel.equalsIgnoreCase("kiss") || commandLabel.equalsIgnoreCase("k")){
			if(args.length == 0){
				player.sendMessage(ChatColor.YELLOW + "[Warning] Not enough arguments.");
			}else if(args.length == 1){
				if(player.getServer().getPlayer(args[0]) != null){
					Player targetplayer = player.getServer().getPlayer(args[0]);
					Location targetlocation = targetplayer.getLocation();
					player.teleport(targetlocation);
					player.addPotionEffect(new PotionEffect(PotionEffectType.HUNGER, getConfig().getInt("hungertime"), getConfig().getInt("hungerlevel")));
					if (getConfig().getBoolean("healplayer")){
						targetplayer.setHealth(20);
						targetplayer.setFireTicks(0);
					}
					targetplayer.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, getConfig().getInt("regentime"), getConfig().getInt("regenlevel")));
					player.sendMessage(ChatColor.RED + "Kissed " + targetplayer.getDisplayName() + ".");
					targetplayer.sendMessage(ChatColor.RED + player.getDisplayName() + " kissed you.");
					return true;
				}else{
					player.sendMessage(ChatColor.YELLOW + "[Warning] Player not online/exist.");
				}
			}else{
				player.sendMessage(ChatColor.YELLOW + "[Warning] Syntax error.");
			}
			
		}else if (commandLabel.equalsIgnoreCase("swap")){
			if (player.isOp()){
				if(args.length == 0){
					player.sendMessage(ChatColor.YELLOW + "[Warning] Not enough arguments.");
				}else if(args.length == 1){
					if(player.getServer().getPlayer(args[0]) != null){
						Player targetplayer = player.getServer().getPlayer(args[0]);
						Location targetlocation = targetplayer.getLocation();
						Location playerlocation = player.getLocation();
						player.teleport(targetlocation);
						targetplayer.teleport(playerlocation);
						player.sendMessage(ChatColor.GREEN + "You swapped place with " + targetplayer.getDisplayName());
						targetplayer.sendMessage(ChatColor.GREEN + "You swapped place with " + player.getDisplayName());
						return true;
					}else{
						player.sendMessage(ChatColor.YELLOW + "[Warning] Player not online/exist.");
					}
				}else if(args.length == 2){
					Player player1 = player.getServer().getPlayer(args[0]);
					Player player2 = player.getServer().getPlayer(args[1]);
					if(player1 != null && player2 != null){
						Location location1 = player1.getLocation();
						Location location2 = player2.getLocation();
						player1.teleport(location2);
						player2.teleport(location1);
						player1.sendMessage(ChatColor.GREEN + "You swapped place with " + player2.getDisplayName());
						player2.sendMessage(ChatColor.GREEN + "You swapped place with " + player1.getDisplayName());
						return true;
					}
				}else{
					player.sendMessage(ChatColor.YELLOW + "[Warning] Syntax error.");
				}
			}else{
				player.sendMessage(ChatColor.YELLOW + "[Warning] No permission.");
				return true;
			}
				
		}else if (commandLabel.equalsIgnoreCase("rng") || commandLabel.equalsIgnoreCase("randomnumbergenerator")){
			Random object = new Random();
			if(args.length == 0){
				player.sendMessage(ChatColor.BLUE + getConfig().getString("prefix") + Integer.toString(object.nextInt(getConfig().getInt("defaultmax"))));
				return true;
			}else if(args.length == 1){
				player.sendMessage(ChatColor.BLUE + getConfig().getString("prefix") + Integer.toString(object.nextInt(Integer.parseInt(args[0]))));
				return true;
			}else if(args.length == 2){
				player.sendMessage(ChatColor.BLUE + getConfig().getString("prefix") + Integer.toString(Integer.parseInt(args[0])+object.nextInt(Integer.parseInt(args[1]))));
				return true;
			}else{
				player.sendMessage(ChatColor.YELLOW + "[Warning] Syntax error.");
			}
		}else if (commandLabel.equalsIgnoreCase("tommystatus")||commandLabel.equalsIgnoreCase("ts")){
			if (player.isOp()){
				if (args.length == 0){
					showstatus(player, player);
					return true;
				}else if(args.length == 1){
					Player target = player.getServer().getPlayer(args[0]);
					if (target.isOnline()){
						showstatus(player, target);
						return true;
					}else{
						sender.sendMessage(ChatColor.YELLOW + "[Warning] Player not online/exist.");
					}
				}
			}else{
				player.sendMessage(ChatColor.YELLOW + "[Warning] No permission.");
				return true;
			}
		}else if(commandLabel.equalsIgnoreCase("staffchat")||commandLabel.equalsIgnoreCase("sc")){
			if (player.isOp()){
				if (args.length == 0){
					player.sendMessage(ChatColor.YELLOW + "[Warning] Syntax error.");
				}else{
					for (Player onlinePlayer : Bukkit.getServer().getOnlinePlayers()){
						if (onlinePlayer.isOp()){
							String message = "";
							for (String part : args){
								message = message + part + " ";
							}
							onlinePlayer.sendMessage(ChatColor.GREEN + "[Staff Chat] " + player.getDisplayName() + " >> " + message);
						}
					}
					return true;
				}
			}else{
				player.sendMessage(ChatColor.YELLOW + "[Warning] No permission.");
				return true;
			}
		}
		
		
		return false;
	}
	
	private void showstatus(Player sender, Player target){
		sender.sendMessage(ChatColor.GREEN + "---------- Tommy Status ----------");
		sender.sendMessage(ChatColor.GREEN + "Name: " + ChatColor.GOLD + target.getDisplayName());
		sender.sendMessage(ChatColor.GREEN + "Health: " + ChatColor.GOLD + target.getHealth() + ChatColor.GREEN + "/" + ChatColor.GOLD + target.getMaxHealth());
		sender.sendMessage(ChatColor.GREEN + "Hunger: " + ChatColor.GOLD + target.getFoodLevel());
		sender.sendMessage(ChatColor.GREEN + "Location:");
		sender.sendMessage(ChatColor.GOLD + "x: " + target.getLocation().getBlockX());
		sender.sendMessage(ChatColor.GOLD + "y: " + target.getLocation().getBlockY());
		sender.sendMessage(ChatColor.GOLD + "z: " + target.getLocation().getBlockZ());
		sender.sendMessage(ChatColor.GOLD + "world: " + target.getLocation().getWorld().getName());
		sender.sendMessage(ChatColor.GREEN + "Item in hand: " + ChatColor.GOLD + target.getItemInHand().getTypeId() + "(" + target.getItemInHand().getType().toString() + ")");
		sender.sendMessage(ChatColor.GREEN + "---------- Tommy Status ----------");
	}

}
